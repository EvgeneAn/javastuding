package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;
import java.util.Locale;

public class FilterMysql implements Filter {
	private ArrayList<String> terms = new ArrayList<String>();
	
	private String getTermFilter() {
		String sqlQuery = "SELECT * FROM musics WHERE ";

		int len = terms.size();
		for (int i = 0; i < len; i++) {
			sqlQuery += String.format("%s", terms.get(i));
			sqlQuery += (i < len - 1) ? " AND " : "";
		}
		
		return sqlQuery;
	}
	
	@Override
	public void addTermName(String val) {
		String term = String.format("(Name REGEXP '%s')", val);
		terms.add(term);
	}

	@Override
	public void addTermAuthor(String val) {
		String term = String.format("(Author REGEXP '%s')", val);
		terms.add(term);
	}

	@Override
	public void addTermGenre(String val) {
		String term = String.format("(Genre REGEXP '%s')", val);
		terms.add(term);
	}

	@Override
	public void addTermURL(String val) {
		String term = String.format("(URL REGEXP '%s')", val);
		terms.add(term);
	}

	@Override
	public void addTermFileSize(int val, int operation) {
		String term = "";
		
		switch (operation) {
		case 0: // больше
			term = String.format("(FileSize > %d)", val);
			break;
	
		case 1: // меньше
			term = String.format("(FileSize < %d)", val);
			break;
			
		default: // равно
			term = String.format("(FileSize = %d)", val);
			break;
		}
		
		terms.add(term);
	}

	@Override
	public void addTermDuration(int val, int operation) {
		String term = "";
		
		switch (operation) {
		case 0: // больше
			term = String.format("(Duration > %d)", val);
			break;
	
		case 1: // меньше
			term = String.format("(Duration < %d)", val);
			break;
			
		default: // равно
			term = String.format("(Duration = %d)", val);
			break;
		}
		
		terms.add(term);
	}

	@Override
	public void addTermRating(float val, int operation) {
		String term = "";
		
		switch (operation) {
		case 0: // больше
			term = String.format(Locale.US, "(Rating > %f)", val);
			break;
	
		case 1: // меньше
			term = String.format(Locale.US, "(Rating < %f)", val);
			break;
			
		default: // равно
			term = String.format(Locale.US, "(Rating = %f)", val);
			break;
		}
		
		terms.add(term);
	}

	@Override
	public ArrayList<Music> filter(DataBase db) {
		ArrayList<Music> musics = new ArrayList<Music>();
		
		if (db instanceof DbMysql) {
			DbMysql mysql = (DbMysql)db;
			String sqlCommand = getTermFilter();
			musics = mysql.query(sqlCommand);
		}
		
		return musics;
	}

	private Music selectMusicByNumber(DataBase db, String sqlCommand) {
		ArrayList<Music> countries = new ArrayList<>();
		
		if (db instanceof DbMysql) {
			DbMysql dbMysql = (DbMysql)db;
			countries = dbMysql.query(sqlCommand);
		}
		
		return countries.get(0);
	}
	
	@Override
	public Music findMusicMaxFileSize(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY FileSize DESC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

	@Override
	public Music findMusicMinFileSize(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY FileSize ASC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

	@Override
	public Music findMusicMaxDuration(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY Duration DESC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

	@Override
	public Music findMusicMinDuration(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY Duration ASC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

	@Override
	public Music findMusicMaxRating(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY Rating DESC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

	@Override
	public Music findMusicMinRating(DataBase db) {
		String sqlCommand = "SELECT * FROM musics ORDER BY Rating ASC LIMIT 1";
		return selectMusicByNumber(db, sqlCommand);
	}

}
