package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;
import java.util.function.Predicate;

public class FilterXml implements Filter {
	private ArrayList<Predicate<Music>> predicates;
	
	public FilterXml() {
		predicates = new ArrayList<Predicate<Music>>();
	}	
	
	public void addPredicate(Predicate<Music> predicate) {
		predicates.add(predicate);
	}
	
	public void clearPredicates() {
		predicates.clear();
	}

	@Override
	public void addTermName(String val) {
		String str = val.toLowerCase();
		predicates.add(x -> x.getName().toLowerCase().contains(str));
	}

	@Override
	public void addTermAuthor(String val) {
		String str = val.toLowerCase();
		predicates.add(x -> x.getAuthor().toLowerCase().contains(str));
	}

	@Override
	public void addTermGenre(String val) {
		String str = val.toLowerCase();
		predicates.add(x -> x.getGenre().toLowerCase().contains(str));
	}

	@Override
	public void addTermURL(String val) {
		String str = val.toLowerCase();
		predicates.add(x -> x.getURL().toLowerCase().contains(str));
	}

	/**
	 * Составляет предикат для числового значения.
	 * 	0 - val > getFileSize()
	 * 	1 - area < getFileSize()
	 * 	любое другое число area == getFileSize()
	 * 
	 * @param val число для сравнения
	 * @param operation операция
	 * @return
	 */	
	@Override
	public void addTermFileSize(int val, int operation) {
		Predicate<Music> pred;
		
		switch (operation) {
		case 0: // больше
			pred = x -> x.getFileSize() > val;
			break;

		case 1: // меньше
			pred = x -> x.getFileSize() < val;
			break;
			
		default: // равно
			pred = x -> x.getFileSize() == val;
			break;
		}
		
		predicates.add(pred);
	}

	@Override
	public void addTermDuration(int val, int operation) {
		Predicate<Music> pred;
		
		switch (operation) {
		case 0: // больше
			pred = x -> x.getDuration() > val;
			break;

		case 1: // меньше
			pred = x -> x.getDuration() < val;
			break;
			
		default: // равно
			pred = x -> x.getDuration() == val;
			break;
		}
		
		predicates.add(pred);
	}

	@Override
	public void addTermRating(float val, int operation) {
		Predicate<Music> pred;
		
		switch (operation) {
		case 0: // больше
			pred = x -> x.getRating() > val;
			break;

		case 1: // меньше
			pred = x -> x.getRating() < val;
			break;
			
		default: // равно
			pred = x -> x.getRating() == val;
			break;
		}
		
		predicates.add(pred);
	}

	/**
	 * Связывает все предикаты логическим И
	 * 
	 * @return Predicate<Music> предикат с единым условием
	 */
	private Predicate<Music> getCondition() {
		Predicate<Music> condition = null;
		
		if (predicates.size() > 0) {
			condition = predicates.get(0);
			
			if (predicates.size() > 1) {
				for (int i = 1; i < predicates.size(); i++) {
					condition = condition.and(predicates.get(i));
				}				
			}
		}
		
		return condition;
	}
	
	@Override
	public ArrayList<Music> filter(DataBase db) {
		ArrayList<Music> filterMusic = new ArrayList<Music>();
		Predicate<Music> condition = getCondition();
		
		if (condition != null) {
			ArrayList<Music> musics = db.all();
			
			musics.forEach(item -> {
				if (condition.test(item))
					filterMusic.add(item);
			});			
		}
		
		return filterMusic;
	}
	
	public interface filterMusicNumber {
		boolean compare(Music m1, Music m2);
	}
	
	// Лямбда выражения для сравнения числовых свойств экземпляров.
	public static filterMusicNumber maxRating = (Music m1, Music m2)-> m1.getRating() > m2.getRating();
	public static filterMusicNumber minRating = (Music m1, Music m2)-> m1.getRating() < m2.getRating();
	public static filterMusicNumber maxFileSize = (Music m1, Music m2)-> m1.getFileSize() > m2.getFileSize();
	public static filterMusicNumber minFileSize = (Music m1, Music m2)-> m1.getFileSize() < m2.getFileSize();
	public static filterMusicNumber maxDuration = (Music m1, Music m2)-> m1.getDuration() > m2.getDuration();
	public static filterMusicNumber minDuration = (Music m1, Music m2)-> m1.getDuration() < m2.getDuration();

	/**
	 * Выполняет поиск музыки на основе лябда-выражения filterMusicNumber.
	 */
	public static Music findMusic(FilterXml.filterMusicNumber filter, ArrayList<Music> musics) {
		Music music = null;
		if (musics.size() > 0) {
			music = musics.get(0);
			for (Music m : musics) {
				if (filter.compare(m, music))
					music = m;
			}
		}
		
		return music;
	}
	
	@Override
	public Music findMusicMaxFileSize(DataBase db) {
		return findMusic(maxFileSize, db.all());
	}

	@Override
	public Music findMusicMinFileSize(DataBase db) {
		return findMusic(minFileSize, db.all());
	}

	@Override
	public Music findMusicMaxDuration(DataBase db) {
		return findMusic(maxDuration, db.all());
	}

	@Override
	public Music findMusicMinDuration(DataBase db) {
		return findMusic(minDuration, db.all());
	}
	
	@Override
	public Music findMusicMaxRating(DataBase db) {
		return findMusic(maxRating, db.all());
	}

	@Override
	public Music findMusicMinRating(DataBase db) {
		return findMusic(minRating, db.all());
	}
}
