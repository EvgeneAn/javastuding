package ru.bstu.it32.anistratov.lab5;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class DbMysql implements DataBase {
	private String url;
	private String username;
	private String password;
	
	private Connection connection;
	private boolean JDBCDriverReady = false;

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the jDBCDriverReady
	 */
	public boolean isJDBCDriverReady() {
		return JDBCDriverReady;
	}

	/**
	 * Загружает драйвер JDBC
	 * @return false - в случае неудачи, иначе true
	 */
	public boolean loadJDBCDriver() {
		boolean flag = true;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			JDBCDriverReady = true;
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		
		return flag;
	}
	
	/**
	 * Устанавливает соединение с БД. JDBC драйвер должен быть загружен.
	 * @return
	 */
	private boolean connect() {
		boolean flag = true;
		
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			flag = false;
			e.printStackTrace();
		}
		
		return flag;
	}
	
	private boolean closeConnection() {
		boolean flag = true;
		
		try {
			connection.close();			
		} catch (SQLException e) {
			e.printStackTrace();
			flag = false;
		}
		
		return flag;
	}
	
	@Override
	public ArrayList<Music> all() {
		ArrayList<Music> musics = new ArrayList<Music>();
		
		if (this.connect()) {
			
			try {
				String sqlCommand = "SELECT * FROM musics";
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				musics = readRows(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return musics;
	}
	
	@Override
	public boolean insert(ArrayList<Music> musics) {
		boolean flag = true;
		
		if (musics.size() == 0) return false;
		
		if (this.connect()) {
			
			try {
				String sqlCommand = "INSERT INTO musics(Name, Author, "
						+ "Genre, URL, FileSize, Duration, Rating) VALUES ";
				Statement statement = connection.createStatement();
				
				int len = musics.size();
				for (int i = 0; i < len; i++) {
					Music item = musics.get(i);
					String sqlValue = String.format(Locale.US, "('%s', '%s', '%s', '%s', %d, %d, %f)",
							item.getName(), item.getAuthor(),
							item.getGenre(), item.getURL(),
							item.getFileSize(), item.getDuration(),
							item.getRating());
					String separator = (i < len - 1) ? ", " : "";
					sqlCommand += sqlValue + separator;
				}
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		return flag;
	}
	
	@Override
	public boolean update(Music music) {
		boolean flag = true;
				
		if (this.connect()) {
			
			try {
				String sqlCommand = "UPDATE musics SET ";
				Statement statement = connection.createStatement();

				String sqlValue = String.format(Locale.US, "Name = '%s', Author "
						+ "= '%s', Genre = '%s', URL = '%s', FileSize = %d,"
						+ " Duration = %d, Rating = %f",
						music.getName(), music.getAuthor(),
						music.getGenre(), music.getURL(),
						music.getFileSize(), music.getDuration(),
						music.getRating());
				sqlCommand += sqlValue + " WHERE Id = " + music.getId();
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		return flag;
	}
	
	@Override
	public boolean delete(int id) {
		boolean flag = true;
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "DELETE FROM musics WHERE Id = " + id;
				
				if (statement.executeUpdate(sqlCommand) == 0) flag = false;
				
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return flag;
	}
	
	@Override
	public boolean truncate() {
		boolean flag = true;
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "truncate table musics";
				statement.execute(sqlCommand);
				statement.close();
			} catch (SQLException e) {
				flag = false;
				e.printStackTrace();
			}
			
			this.closeConnection();
		} else
			flag = false;
		
		return flag;
	}
	
	@Override
	public Music getRow(int id) {
		ArrayList<Music> musics = new ArrayList<Music>();
		
		if (this.connect()) {
			
			try {
				Statement statement = connection.createStatement();
				String sqlCommand = "SELECT * FROM musics WHERE Id = " + id;
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				musics = readRows(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();
		}
		
		return musics.get(0);

	}
	
	@Override
	public String toString() {
		return "БД MySQL";
	}
	
	@Override
	public Filter createFilter() {
		return new FilterMysql();
	}

	private static ArrayList<Music> readRows(ResultSet rs) throws SQLException {
		ArrayList<Music> musics = new ArrayList<Music>();
		
		while (rs.next()) {
			Music music = new Music();
			
			music.setId(rs.getInt("Id"));
			music.setName(rs.getString("Name"));
			music.setAuthor(rs.getString("Author"));
			music.setGenre(rs.getString("Genre"));
			music.setURL(rs.getString("URL"));
			music.setFileSize(rs.getInt("FileSize"));
			music.setDuration(rs.getInt("Duration"));
			music.setRating(rs.getFloat("Rating"));
			
			musics.add(music);
		}
		
		return musics;
	}
	
	public ArrayList<Music> query(String sqlCommand) {
		ArrayList<Music> musics = new ArrayList<Music>();
		
		if (this.connect()) {
			try {
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(sqlCommand);
				
				musics = readRows(rs);
				
				rs.close();
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			this.closeConnection();

		}
		
		return musics;
	}
}
