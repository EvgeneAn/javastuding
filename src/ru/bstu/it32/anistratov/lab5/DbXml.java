package ru.bstu.it32.anistratov.lab5;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class DbXml implements DataBase {
	private static ArrayList<Music> musics = new ArrayList<Music>();
	private String filePath;
	
	public DbXml(String filePath) {
		this.filePath = filePath;
	}
	
	public String getFilePath() {
		return this.filePath;
	}
	
	public void setFilePath(String filePath) {
		if (!filePath.equals(""))
			this.filePath = filePath;
	}
	
	public ArrayList<Music> getMusics() {
		return musics;
	}
	
	@Override
	public Music getRow(int id) {
		for (Music music : musics) {
			if (music.getId() == id) {
				return music;
			}
		}
		return null;
	}

	
	/**
	 * Создает документ DOM на основе списка musics. Может вернуть
	 * ошибку, которую нужно обрабатывать.
	 *  
	 * @param muscis список музыки
	 * @return документ DOM
	 * @exception ParserConfigurationException
	 */
	public Document getDOM(ArrayList<Music> muscis) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();
		
		Element root = doc.createElement("Musics");
		doc.appendChild(root);
		
		for (Music music : muscis) {
			// создать узел, добавить текст и атрибуты
			Element child = doc.createElement("Music");
			child.setTextContent(music.getName());

			String number = Integer.toString(music.getId());
			child.setAttribute("id", number);
			child.setAttribute("author", music.getAuthor());
			child.setAttribute("genre", music.getGenre());
			child.setAttribute("URL", music.getURL());
			number = Integer.toString(music.getFileSize());
			child.setAttribute("fileSize", number);
			number = Integer.toString(music.getDuration());
			child.setAttribute("duration", number);
			number = Float.toString(music.getRating());
			child.setAttribute("rating", number);
			
			root.appendChild(child);
		}
		
		
		return doc;
	}
	
	/**
	 * Записывает список музыки в xml файл.
	 * 
	 * @param musics массив объектов Music
	 * @return результат работы метода (true/false)
	 */
	public boolean writeMusics(ArrayList<Music> musics) {
		boolean flag = true;
		
		try {
			Document doc = this.getDOM(musics);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			
			DOMSource source = new DOMSource(doc);
			StreamResult file = new StreamResult(new File(this.filePath));
			transformer.transform(source, file);
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		
		return flag;
	}
	
	/**
	 * Считывает все записи из файла и формирует список объектов music
	 * 
	 * @return результат работы метода (true/false)
	 */
	@Override
	public ArrayList<Music> all() {
		musics.clear();
		try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            SAXHandler handler = new SAXHandler();
            
            parser.parse(new File(this.filePath), handler);
        } catch (Exception e) {
        	e.printStackTrace();
		}
		
		return musics;
	}
	
	/**
	 * Метод добавления новых записей в xml файл.
	 * 
	 * @param newMusics список музыки для добавления
	 * @return результат работы метода (true/false)
	 */
	@Override
	public boolean insert(ArrayList<Music> newMusics) {
		this.all();
		if (musics.size() > 0) {
			// определить свободный id после последней записи
			int id = musics.size();
			if (id == 0) id = 1;
			else id = musics.get(id - 1).getId() + 1;

			for (Music music : newMusics) {
				music.setId(id);
				++id;
				musics.add(music);
			}
		} else {
			musics = newMusics;
		}

		return this.writeMusics(musics);
	}
	
	/**
	 * Редактирует запись music в файле xml.
	 * 
	 * @param music редактируемая запись о музыке
	 * @return результат работы метода (true/false)
	 */
	@Override
	public boolean update(Music music) {
		boolean flag = false;
		this.all();
		
		// поиск записи с совпадающим id и его замена
		if (musics.size() > 0) {
			for (int i = 0; i < musics.size(); ++i) {
				if (musics.get(i).getId() == music.getId()) {
					musics.set(i, music);
					flag = true;
					break;
				}
			}			
		}
		
		if (flag) {
			flag = this.writeMusics(musics);
		}
		
		return flag;
	}
	
	/**
	 * Удаляет запись о музыке из xml файла по ее id.
	 * 
	 * @param id идентификатор музыки для удаления
	 * @return результат работы метода (true/false)
	 */
	@Override
	public boolean delete(int id) {
		boolean flag = false;
		this.all();
		
		if (musics.size() > 0) {
			for (int i = 0; i < musics.size(); ++i) {
				if (musics.get(i).getId() == id) {
					musics.remove(i);
					flag = true;
					break;
				}
			}			
		}
		
		if (flag) {
			flag = this.writeMusics(musics);
		}
		
		return flag;
	}
	
	/**
	 * Удаляет все записи в файле xml
	 */
	@Override
	public boolean truncate() {
		musics.clear();
		return this.writeMusics(musics);
	}
	
	@Override
	public String toString() {
		return "БД XML";
	}
	
	@Override
	public Filter createFilter() {
		return new FilterXml();
	}

	/**
	 * Обработчик для SAX. Создает объекты music и добавляет их в список musics
	 */
	private static class SAXHandler extends DefaultHandler {
		private String lastElement, name;
		private Music music;
		
	    @Override
	    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	        lastElement = new String(qName);
	        music = new Music();
	        
	    	if (qName.equals("Music")) {
	    		String atr = attributes.getValue("id");
	    		int number = 0; 
	    		
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	    		music.setId(number);
	    		
	    		atr = attributes.getValue("duration");
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	    		music.setDuration(number);
	    		
	    		atr = attributes.getValue("rating");
	    		float fNumber;
	    		try {
	    			fNumber = Float.parseFloat(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			fNumber = 0;
				}
	        	music.setRating(fNumber);
	        	
	        	atr = attributes.getValue("fileSize");
	    		try {
	    			number = Integer.parseInt(atr);
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			number = 0;
				}
	        	music.setFileSize(number);
	        	
	        	atr = attributes.getValue("author");
	        	music.setAuthor(atr);
	        	
	        	atr = attributes.getValue("URL");
	        	music.setURL(atr);
	        	
	        	atr = attributes.getValue("genre");
	        	music.setGenre(atr);
	        }
	    }
	    
        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            name = new String(ch, start, length);
            
            name = name.replace("\n", "").trim();
            
            if (!name.isEmpty())
                if (lastElement.equals("Music"))
                	music.setName(name);
        }
	    
        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equals("Music")) {
            	musics.add(music);
            }
        }
	}
}
