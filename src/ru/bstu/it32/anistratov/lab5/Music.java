package ru.bstu.it32.anistratov.lab5;

/**
 * Класс, представляющий 1 запись о музыкальном произведении.
 */
public class Music {
	private int id;
	private String name;
	private String author;
	private String genre; // Жанр
	private String URL;   // Ссылка на скачивание
	private int fileSize;
	private int duration; // Продолжительность
	private float rating;
	
	public Music() {
		this(0, "", "", "", "", 0, 0, 0);
	};
	
	public Music(int id, String name, String author, String genre, String URL,
				 int fileSize, int duration, float rating) {
		if (id < 1)
			id = 0;
		if (name.equals(""))
			name = "[No name]";
		if (author.equals(""))
			author = "[No author]";
		if (genre.equals(""))
			genre = "[No genre]";
		if (URL.equals(""))
			URL = "[No URL]";
		if (fileSize < 0)
			fileSize = 0;
		if (duration < 0)
			duration = 0;
		if (rating < 0)
			rating = 0;
		
		this.id = id;
		this.name = name;
		this.author = author;
		this.genre = genre;
		this.URL = URL;
		this.fileSize = fileSize;
		this.duration = duration;
		this.rating = rating;
		
	}

	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		if (id < 0)
			this.id = 0;
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (!name.equals(""))
			this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		if (!author.equals(""))
			this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		if (!genre.equals(""))
			this.genre = genre;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		if (!URL.equals(""))
			this.URL = URL;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		if (fileSize >= 0)
			this.fileSize = fileSize;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		if (duration >= 0)
			this.duration = duration;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		if (rating >= 0)
			this.rating = rating;
	}

	@Override
	public String toString() {
		return "id: " + id + "\nНазвание: " + name + ", автор: " + author + ", жанр: " + genre
				+ "\nСсылка для скачивания: " + URL + "\nРазмер файла: " + fileSize
				+ "\nПродолжительность: " + duration + "\nРейтинг: " + rating;
	}
}
