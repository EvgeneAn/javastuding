package ru.bstu.it32.anistratov.lab5;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.util.function.Predicate;

public class Program {
	private static int MIN_NUMBER;
	private static int MAX_SIZE;
	private static String FILE_XML;
	private static String WARN_MUSIC_NOT_FOUND;
	private static String WARN_JDBC_DRIVER;
	private static String MAIN_MENU;
	private static String FILTER_MENU;
	private static String DB_URL;
	private static String DB_USERNAME;
	private static String DB_PASSWORD;
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		getProperties();
		
		DbMysql dbMysql = new DbMysql();
		dbMysql.setUrl(DB_URL);
		dbMysql.setUsername(DB_USERNAME);
		dbMysql.setPassword(DB_PASSWORD);
		
		if (!dbMysql.loadJDBCDriver()) {
			System.out.println(WARN_JDBC_DRIVER);
		}

		DbXml xml = new DbXml(FILE_XML);
		DataBase currentDb = xml;

		boolean flag = true;
		while(flag) {
			showMenu(currentDb);
			int choice = getNumber(scan, 0, "Выберите действие: ");
			
			switch (choice) {
			case 0: // Выход
				flag = false;
				break;
			case 1: // Показать записи из БД
				showData(currentDb);				
				break;
			case 2: // Добавление записи
				insertMusic(scan, currentDb);
				break;
			case 3: // Изменение записи
				updateMusic(scan, currentDb);
				break;
			case 4: // Удаление записи
				deleteMusic(scan, currentDb);
				break;
			case 5: // Поиск по параметрам
				filterMusics(scan, currentDb);
				break;
			case 6: // Смена активной БД
				currentDb = changeDb(currentDb, xml, dbMysql);
				break;
			case 7: // Конверитрование данных
				convertData(currentDb, xml, dbMysql);
				break;
			default:
				System.out.println("Нет такого действия");
				break;
			}
		}

		System.out.println("Конец программы");
		scan.close();

	}

	public static void getProperties() {
		Properties prop = new Properties();
        try {
            //обращение к файлу и получение данных
            FileInputStream fis = new FileInputStream("./resourсes/config.properties");
            prop.load(fis);
            
            FILE_XML = prop.getProperty("fileXml", "");
            
            WARN_MUSIC_NOT_FOUND = new String(prop.getProperty("warnMusicNotFound", "").getBytes("ISO8859-1"));
            
            WARN_JDBC_DRIVER = new String(prop.getProperty("warnJDBCDriver", "").getBytes("ISO8859-1"));
            
    		MAIN_MENU = new String(prop.getProperty("infoMainMenu", "").getBytes("ISO8859-1"));
    		
    		FILTER_MENU = new String(prop.getProperty("infoFilterMenu", "").getBytes("ISO8859-1"));
    		
    		DB_URL = new String(prop.getProperty("dbUrl", "").getBytes("ISO8859-1"));
    		
    		DB_USERNAME = new String(prop.getProperty("dbUsername", "").getBytes("ISO8859-1"));
    		
    		DB_PASSWORD = new String(prop.getProperty("dbPassword", "").getBytes("ISO8859-1"));
    		
    		String value = prop.getProperty("minNumber");
    		try {
            	MIN_NUMBER = Integer.parseInt(value);
            } catch (Exception e) {
            	e.printStackTrace();
			}

    		value = prop.getProperty("maxSize");
    		try {
            	MAX_SIZE = Integer.parseInt(value);
            } catch (Exception e) {
            	e.printStackTrace();
			}	
        } catch (IOException e) {
            System.out.println("Ошибка в программе: файл .properties не найден");
            e.printStackTrace();
        }
	}
	
	private static void showData(DataBase db) {
		db.all();
		
		ArrayList<Music> musics = db.all();
		
		for (Music country : musics) {
			System.out.println("");
			System.out.println(country);
		}
	}

	/**
	 * Считывает число с консоли, которое больше либо равно min
	 * @param scanner
	 * @param min минимальное число
	 * @param msg сообщение для приглашения ввода
	 * @return
	 */
	private static int getNumber(Scanner scanner, int min, String msg) {
		int number = min;
		String str = "";
		while(true) {
			System.out.print(msg);
			str = scanner.nextLine();
			try {
				number = Integer.parseInt(str);
				if (number >= min) break;
				else System.out.printf("Значение должно быть больше либо "
						               + "равно%s\n", min);
			} catch (Exception e) {
				System.out.printf("Не удалось преобразовать %s в число\n", str);
			}			
		}
		return number;
	}
	
	private static float getFNumber(Scanner scanner, float min, String msg) {
		float number = min;
		String str = "";
		while(true) {
			System.out.print(msg);
			str = scanner.nextLine();
			try {
				number = Float.parseFloat(str);
				if (number >= min) break;
				else System.out.printf("Значение должно быть больше либо "
						               + "равно%s\n", min);
			} catch (Exception e) {
				System.out.printf("Не удалось преобразовать %s в число\n", str);
			}			
		}
		return number;
	}
	
	private static void showMenu(DataBase db) {
		System.out.println("");
		System.out.println("----" + db + "----");
		System.out.println(MAIN_MENU);
		System.out.println("----------------");
		System.out.println("");
	}

	private static void showMenuFilterNumber() {
		System.out.println("");
		System.out.println(FILTER_MENU);
		System.out.println("");
	}
	
	private static boolean getYesOrNo(Scanner scan) {
		boolean yesNo = false;
		
		while(true) {
			System.out.println("0 - да\n1 - нет");
			int number = getNumber(scan, 0, "Введите значение: ");
			
			if (number == 0) {
				yesNo = true;
				break;
			} else if (number == 1) {
				yesNo = false;
				break;
			}
		}
		
		return yesNo;
	}

	/**
	 * Считывает с консоли информацию о музыке
	 * @param scan
	 * @return объект Music
	 */
	private static Music getMusic(Scanner scan) {
		Music music = new Music();
		
		System.out.print("Введите название: ");
		music.setName(scan.nextLine());
		
		System.out.print("Введите автора: ");
		music.setAuthor(scan.nextLine());
		
		System.out.print("Введите жанр: ");
		music.setGenre(scan.nextLine());
		
		System.out.print("Введите ссылку для скачивания: ");
		music.setURL(scan.nextLine());
		
		int number;
		while (true) {
			number = getNumber(scan, MIN_NUMBER, "Введите размер файла (байт): ");
			if (number > MAX_SIZE)
				System.out.println("Максимальный размер файла должен быть не больше "
					+ MAX_SIZE + " байт.");
			else {				
				music.setFileSize(number);
				break;
			}
		}
		
		number = getNumber(scan, MIN_NUMBER, "Введите продолжительность (с.): ");
		music.setDuration(number);
		
		float fNumber = getFNumber(scan, 0, "Введите рейтинг: ");
		music.setRating(fNumber);
		
		return music;
	}
	
	/**
	 * Спрашивает новые значения полей с консоли и изменяет объект music
	 * @param scan
	 * @param country
	 */
	private static void editMusic(Scanner scan, Music music) {
		System.out.println("Страна:\n\n" + music + "\n");
		
		System.out.println("Изменить название?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите название: ");
			music.setName(scan.nextLine());
		}
		
		System.out.println("Изменить автора?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите автора: ");
			music.setAuthor(scan.nextLine());
		}
		
		System.out.println("Изменить жанр?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите жанр: ");
			music.setGenre(scan.nextLine());
		}
		
		System.out.println("Изменить ссылку для скачивания?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите ссылку: ");
			music.setURL(scan.nextLine());
		}
		
		System.out.println("Изменить размер файла?");
		if (getYesOrNo(scan)) {
			int number;
			while (true) {
				number = getNumber(scan, MIN_NUMBER, "Введите размер файла (байт): ");
				if (number > MAX_SIZE)
					System.out.println("Максимальный размер файла должен быть не больше "
						+ MAX_SIZE + " байт.");
				else {				
					music.setFileSize(number);
					break;
				}
			}
		}
		
		System.out.println("Изменить продолжительность?");
		if (getYesOrNo(scan)) {
			int number = getNumber(scan, MIN_NUMBER, "Введите продолжительность (с.): ");
			music.setDuration(number);
		}
		
		System.out.println("Изменить рейтинг?");
		if (getYesOrNo(scan)) {
			float number = getFNumber(scan, 0, "Введите рейтинг: ");
			music.setRating(number);
		}
	}

	private static void insertMusic(Scanner scan, DataBase db) {
		ArrayList<Music> musics = new ArrayList<>();
		Music music = getMusic(scan);
		musics.add(music);
		db.insert(musics);
	}

	private static void deleteMusic(Scanner scan, DataBase db) {
		int number = getNumber(scan, MIN_NUMBER, "Введите id: ");
		if (!db.delete(number))
			System.out.println("Нет записи с id = " + number + "\n");
		else
			System.out.println("Запись была удалена\n");
	}

	private static void updateMusic(Scanner scan, DataBase db) {
		int number = getNumber(scan, MIN_NUMBER, "Введите id: ");
		db.all();
		Music music = db.getRow(number);
		
		if (music != null) {
			editMusic(scan, music);
			db.update(music);
		}
		else
			System.out.println("Нет записи с id = " + number + "\n");
	}
	
	private static Filter getFilterMusic(Scanner scan, DataBase db) {
		Filter filter = db.createFilter();
		
		System.out.println("Поиск по названию?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите название: ");
			String str = scan.nextLine();
			filter.addTermName(str);
		}
		
		System.out.println("Поиск по автору?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите имя автора: ");
			String str = scan.nextLine();
			filter.addTermAuthor(str);
		}
		
		System.out.println("Поиск по жанру?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите название жанра: ");
			String str = scan.nextLine();
			filter.addTermGenre(str);
		}
		
		System.out.println("Поиск по ссылке для скачивания?");
		if (getYesOrNo(scan)) {
			System.out.print("Введите ссылку : ");
			String str = scan.nextLine();
			filter.addTermURL(str);
		}
		
		System.out.println("Поиск по размеру файла?");
		if (getYesOrNo(scan)) {
			showMenuFilterNumber();
			int operation = getNumber(scan, 0, "Выберите операцию: ");
			
			int number;
			while (true) {
				number = getNumber(scan, MIN_NUMBER, "Введите размер файла (байт): ");
				if (number > MAX_SIZE)
					System.out.println("Максимальный размер файла должен быть не больше "
						+ MAX_SIZE + " байт.");
				else break;
			}
			
			filter.addTermFileSize(number, operation);
		}
		
		System.out.println("Поиск по продолжительности?");
		if (getYesOrNo(scan)) {
			showMenuFilterNumber();
			int operation = getNumber(scan, 0, "Выберите операцию: ");
			int number = getNumber(scan, MIN_NUMBER, "Введите продолжительность (с.): ");
			
			filter.addTermDuration(number, operation);
		}
		
		System.out.println("Поиск по рейтингу?");
		if (getYesOrNo(scan)) {
			showMenuFilterNumber();
			int operation = getNumber(scan, 0, "Выберите операцию: ");
			float number = getFNumber(scan, 0, "Введите рейтинг: ");
			
			filter.addTermRating(number, operation);
		}
		
		return filter;
	}
	
	private static Music getMaxMinMusicFilterx(Scanner scan, DataBase db) {
		System.out.println("1 - Поиск музыки с максимальным размером");
		System.out.println("2 - Поиск музыки с минимальным размером");
		System.out.println("3 - Поиск музыки с максимальной продолжительностью");
		System.out.println("4 - Поиск музыки с минимальной продолжительностью");
		System.out.println("5 - Поиск музыки с максимальным рейтингом");
		System.out.println("6 - Поиск музыки с минимальным рейтингом");
		
		Filter filter = db.createFilter();
		Music music;
		
		while (true) {
			int number = getNumber(scan, 1, "Введите число: ");
			
			if (number == 1) {
				music = filter.findMusicMaxFileSize(db);
				break;
			}
			
			if (number == 2) {
				music = filter.findMusicMinFileSize(db);
				break;
			}
			
			if (number == 3) {
				music = filter.findMusicMaxDuration(db);
				break;
			}
			
			if (number == 4) {
				music = filter.findMusicMinDuration(db);
				break;
			}
			
			if (number == 5) {
				music = filter.findMusicMaxRating(db);
				break;
			}
			
			if (number == 6) {
				music = filter.findMusicMinRating(db);
				break;
			}
		}
		
		return music;
	}


	private static void filterMusics(Scanner scan, DataBase db) {
		System.out.println("Поиск по нескольким параметрам?");
		
		if (getYesOrNo(scan)) {
			Filter filter = getFilterMusic(scan, db);
			
			ArrayList<Music> musics = filter.filter(db);
			
			System.out.println("Найденная музыка");
			
			if (musics.size() == 0) {
				System.out.println(WARN_MUSIC_NOT_FOUND);
			} else {
				for (Music music : musics) {
					System.out.println(music);
				}				
			}
		} else {			
			Music music = getMaxMinMusicFilterx(scan, db);
			System.out.println(music);
		}
	}

	private static DataBase changeDb(DataBase curDb, DbXml dbXml, DbMysql Mysql) {
		DataBase newDb = null;
		
		if (curDb instanceof DbXml) {
			if (Mysql.isJDBCDriverReady())
				newDb = Mysql;
			else {
				System.out.println(WARN_JDBC_DRIVER);
				newDb = dbXml;
			}
		} else
			newDb = dbXml;
		
		return newDb;
	}
	
	private static void convertData(DataBase curDb, DbXml dbXml, DbMysql Mysql) {
		boolean flag = false;
		
		if (Mysql.isJDBCDriverReady()) {
			if (curDb instanceof DbXml)
				flag = DataBase.convertData(curDb, Mysql);
			else
				flag = DataBase.convertData(curDb, dbXml);
		} else {
			System.out.println(WARN_JDBC_DRIVER);
		}
		
		if (flag) {
			String message = String.format("Конвертация данных из %s в %s прошла успешно",
					curDb, (curDb instanceof DbXml) ? Mysql : dbXml);
			System.out.println(message);
		}
	}

}
