package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;

interface DataBase {
	/**
	 * Считывает все записи из БД и возвращает список стран
	 * @return ArrayList Country
	 */
	public ArrayList<Music> all();
	
	public boolean insert(ArrayList<Music> musics);
	public boolean update(Music music);
	public boolean delete(int id);
	public boolean truncate();
	public Music getRow(int id);
	public String toString();
	
	public static boolean convertData(DataBase dbFrom, DataBase dbTo) {
		boolean flag = false;
		
		ArrayList<Music> countries = dbFrom.all();
		
		if (dbTo.truncate()) {
			if (dbTo.insert(countries)) flag = true;
		}
		
		return flag;
	}

	public Filter createFilter();
}
