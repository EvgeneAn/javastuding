package ru.bstu.it32.anistratov.lab5;

import java.util.ArrayList;

interface Filter {
	public void addTermName(String val);
	public void addTermAuthor(String val);
	public void addTermGenre(String val);
	public void addTermURL(String val);
	public void addTermFileSize(int val, int operation);
	public void addTermDuration(int val, int operation);
	public void addTermRating(float val, int operation);
	
	/**
	 * Фильтрует элементы с заданными условиями
	 * 
	 * @param db база данных
	 * @return ArrayList<Music> отфильтрованные элементы
	 */
	public ArrayList<Music> filter(DataBase db);
	
	public Music findMusicMaxFileSize(DataBase db);
	public Music findMusicMinFileSize(DataBase db);
	public Music findMusicMaxDuration(DataBase db);
	public Music findMusicMinDuration(DataBase db);
	public Music findMusicMaxRating(DataBase db);
	public Music findMusicMinRating(DataBase db);
}
