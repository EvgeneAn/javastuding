package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class Program {
	
	/** command считанное число с консоли */
	static int command;
	
	/**
	 * Логика основной программы. Определяет, откуда считывать данные,
	 * затем выполняет задания.
	 * 
	 * @param args аргументы запуска
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String[] tasks = {"task1.txt", "task2.txt", "task3.txt", "task4.txt"};
		
		System.out.println("Для ввода данных с консоли введите 0. Для ввода "
				           + "данных с файла введите 1.");
		if (!getCnslNumber(in)) {
			System.out.println("Конец программы!");
			return;
		}
		
		Geometry g = new Geometry();
		Seasons seasons = new Seasons();
		MixArray mixArray;
		
		if (command == 0) {
			// задание 1
			System.out.println("\nВывод тригонометрических функций.");
			g.setCnslNumber(in);
			displayMathFunc(g);
			
			// задание 2
			System.out.println("\nВремена года.");
			System.out.println("Введите номер времени года (1-4).");
			if (getCnslNumber(in)) {
				seasons.displaySeason(command);
			}
			
			// задание 3
			System.out.println("\nТрехзначные числа с разными цифрами.");
			if (getCnslNumber(in)) {
				Numbers.numbersDisplay(command);
			}
			
			// задание 4
			System.out.println("\nПеремешивание массива.");
			mixArray = new MixArray();
			mixArray.getConsoleArray(in);
			
			System.out.print("Вы ввели: ");
			mixArray.displayInfo();
			mixArray.mixing();
			
			System.out.print("Результат перемешивания: ");
			mixArray.displayInfo();
		} else if (command == 1) {
			// задание 1
			System.out.println("\nВывод тригонометрических функций.");
			if (g.setFileNumber(tasks[0])) {
				displayMathFunc(g);
			} else {
				System.out.printf("\nНе удалось считать данные из файла %s.\n",
				                  tasks[0]);
			}
			
			// задание 2
			System.out.println("\nВремена года.");
			if (getFileNumber(tasks[1])) {
				seasons.displaySeason(command);
			} else {
				System.out.printf("\nНе удалось считать данные из файла %s.\n",
				          tasks[1]);
			}
			
			// задание 3
			System.out.println("\nТрехзначные числа с разными цифрами.");
			if (getFileNumber(tasks[2])) {
				Numbers.numbersDisplay(command);
			} else {
				System.out.printf("\nНе удалось считать данные из файла %s.\n",
						          tasks[2]);
			}
			
			// задание 4
			System.out.println("\nПеремешивание массива.");
			mixArray = new MixArray();
			mixArray.getFileArray(tasks[3]);
			
			System.out.print("Был считан массив: ");
			mixArray.displayInfo();
			mixArray.mixing();
			System.out.print("Результат перемешивания: ");
			mixArray.displayInfo();
		}
		
		System.out.println("\nКонец программы!");
		in.close();
	}

	/**
	 * Считывает с консоли целое число и записывает его в поле number.
	 * 
	 * @param in - экземпляр класса Scanner
	 * @return true - успешное считывание, false - считывание пропущено
	 */
	public static boolean getCnslNumber(Scanner in) {
		boolean res = false;
		String buf;
		
		System.out.println("Чтобы пропустить ввод, введите exit.");
		System.out.print("Введите целое число: ");
		
		while (true) {
			if (in.hasNextInt()) {
				command = in.nextInt();
				res = true;
				break;
			}
			
			buf = in.next();
			if (buf.equals("exit")) {
				System.out.println("Вы пропустили ввод.");
				break;	
			}
			
			System.out.print("Ввели некорректное число. Попробуйте еще раз: ");
		}
		
		return res;
	}
	
	/**
	 * Считывает целое число из файла и записывает его в поле number.
	 * 
	 * @param filePath путь к файлу
	 * @return true - успешное считывание, false - считывание пропущено 
	 */
	public static boolean getFileNumber(String filePath) {
		boolean res = false;
		
		try (FileReader reader = new FileReader(new File(filePath));
			 Scanner in = new Scanner(reader)) {
			if (!in.hasNextInt()) {
				throw new Exception("В файле содержаться некорректные "
									+ "данные!");
			}

			command = in.nextInt();
			res = true;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		return res;
	}
	
	/**
	 * Выводит значение математических функций (sin, cos, ln) для поля number
	 * в порядке возрастания.
	 */
	public static void displayMathFunc(Geometry obj) {
		MathValue[] values = obj.getMathFunc();
		
		for (MathValue value : values) {
			value.displayMessage(obj.getNumber());
		}
	}
}
