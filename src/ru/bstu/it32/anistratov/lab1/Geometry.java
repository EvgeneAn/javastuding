package ru.bstu.it32.anistratov.lab1;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileReader;
import java.io.File;

public class Geometry {
	static final Logger Logger = LogManager.getLogger();
	
	/** number число для обработки методами */
	private double number;
	
	public double getNumber() {
		return number;
	}

	/** Инициализирует данные значениями по умолчанию. */
	public Geometry() {
		Logger.info("Создан объект класса Geometry");
		number = 0;
	}
	
	/** Инициализирует данные значениями по умолчанию. */
	public Geometry(double number) {
		Logger.info("Создан объект класса Geometry");
		this.number = number;
	}
		
	/**
	 * Считывает с консоли вещественное число и записывает его в поле number.
	 * 
	 * @param in - экземпляр класса Scanner
	 * @return true - успешное считывание, false - считывание пропущено
	 */
	public boolean setCnslNumber(Scanner in) {
		boolean res = false;
		
		System.out.println("Чтобы пропустить ввод, введите exit.");
		System.out.print("Введите вещественное число: ");
		
		while (true) {
			if (in.hasNextDouble()) {
				number = in.nextDouble();
				res = true;
				break;
			}
			
			String buf = in.nextLine();
			if (buf.equals("exit")) {
				System.out.println("Вы пропустили ввод.");
				break;	
			}
			
			System.out.print("Ввели некорректное число. Попробуйте еще раз: ");
		}
		
		return res;
	}
	
	/**
	 * Считывает вещественное число из файла и записывает его в поле number.
	 * 
	 * @param filePath путь к файлу
	 * @return true - успешное чтение файла, false - ошибка при работе с файлом
	 */
	public boolean setFileNumber(String filePath) {
		boolean res = false;
		
		try (FileReader reader = new FileReader(new File(filePath));
			 Scanner in = new Scanner(reader)) {
			if (!in.hasNextDouble()) {
				String buf, message;
				
				buf = in.nextLine();
				message = String.format("Задание 1. Файл %s содержит некорректные"
						+ "данные: %s", filePath, buf);
				throw new Exception(message);
			}
			
			number = in.nextDouble();
			res = true;
		} catch (Exception ex) {
			Logger.error(ex.getMessage(), ex);
			System.out.println(ex.getMessage());
		}
		
		return res;
	}
	
	/**
	 * Генерирует массив со значениями математических функций и сортирует его.
	 * 
	 * @return массив со значениями математических функций
	 */
	public MathValue[] getMathFunc() {
		MathValue[] values = new MathValue[3];
		values[0] = new MathValue(Math.sin(number), "sin");
		values[1] = new MathValue(Math.cos(number), "cos");
		values[2] = new MathValue(Math.log10(number), "ln");
		
		// сортировока значений по возрастанию
		for (int i = 0; i < values.length; ++i) {
			for (int j = i + 1; j < values.length; ++j) {
				if (values[i].value > values[j].value) {
					MathValue buf = values[i];
					values[i] = values[j];
					values[j] = buf;
				}
			}
		}
		
		return values;
	}
	
	/**
	 * Генерирует массив с сообщениями для математических функций.
	 * 
	 * @param number исходное значение
	 * @return массив с сообщениями
	 */
	public String[] getMessages(double number) {
		MathValue[] values = getMathFunc();
		
		String[] messages = new String[values.length];
		
		for (int i = 0; i < values.length; ++i) {
			messages[i] = values[i].getMessage(number);
		}
		
		return messages;
	}
}

/**
 * Класс, который хранит результат математической функции и имя
 * математической функции.
 */
class MathValue {
	static final Logger Logger = LogManager.getLogger();
	
	/** value значение математической функции */
	double value;
	
	/** funcName имя математической функции */
	String funcName;
	
	/** Инициализирует поля объекта данными. */
	MathValue(double value, String funcName) {
		this.value = value;
		this.funcName = funcName;
	}
	
	/**
	 * Выводит результат математической функции в консоль.
	 * 
	 * @param number исходное значение
	 */
	public void displayMessage(double number) {
		if (Double.isNaN(value)) {
			String message = String.format("Задание 1. Для числа %f %s(x) "
					+ "невозможно определить.", number, funcName);
			System.out.println(message);
			Logger.warn(message);
		} else {
			System.out.printf("%s(%f) = %f\n", funcName, number, value);
		}
	}
	
	/**
	 * Возвращает строку с результатом математической функции.
	 * 
	 * @param number исходное значение
	 */
	public String getMessage(double number) {
		if (Double.isNaN(value)) {
			String message = String.format("Задание 1. Для числа %f %s(x) "
					+ "невозможно определить.", number, funcName);
			System.out.println(message);
			Logger.warn(message);
			return String.format("Для числа %f %s(x) невозможно определить.",
					          	 number, funcName);
		} else {
			return String.format("%s(%f) = %f.", funcName, number, value);
		}
	}
}