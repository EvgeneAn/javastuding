package ru.bstu.it32.anistratov.lab1;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Класс, представляющий время года.
 */
public class TimeYear {
	static final Logger Logger = LogManager.getLogger();
	
	/** name название времени года */
	private String name;
	
	/** months массив с названиями месяцев времени года */
	private String[] months;
	
	/** monthDays массив с кол-вом дней месяцев */
	private int[] monthDays;
	
	public String getName() {
		return name;
	}

	public String[] getMonths() {
		return months;
	}

	public int[] getMonthDays() {
		return monthDays;
	}
	
	/**
	 * Принимает название времени года, массив с названиями месяцев времени
	 * года и массив с количеством дней каждого месяца. Если длины массивов
	 * больше 3, то они будут проинициализированы значениями по умолчанию.
	 * 
	 * @param name название времени года
	 * @param months массив с названиями месяцов времени года
	 * @param monthDays массив с количеством дней месяцов
	 */
	TimeYear(String name, String[] months, int[] monthDays) {
		this.name = name;
		
		if (months.length == 3) {
			this.months = Arrays.copyOf(months, months.length);
		} else {
			this.months = new String[3];
		}
		
		if (months.length == 3) {
			this.monthDays = Arrays.copyOf(monthDays, monthDays.length);
		} else {
			this.monthDays = new int[3];
		}
		
		Logger.info("Создан объект класса TimeYear.");
	}

	/** Выводит информацию о времени года. */
	public void displayInfo() {
		System.out.printf("Время года: %s\n", name);
		
		for (String season : months) {
			System.out.printf("%10s ", season);
		}
		System.out.println();
		
		for (int days : monthDays) {
			System.out.printf("%10d ", days);
		}
		System.out.println();
	}
}