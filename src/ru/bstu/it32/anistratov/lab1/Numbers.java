package ru.bstu.it32.anistratov.lab1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Numbers {
	static final Logger Logger = LogManager.getLogger();
	
	/**
	 * Генерирует строку всех трехзначных чисел, у которых все цифры
	 * разные. Генерирует до n.
	 * 
	 * @param n трехзначное число.
	 */
	public static String threeNumbers(int n) {
		String res = "";
		if ((n < 100) || (n > 999)) {
			String message = String.format("Задание 3. Введено не трехзначное число: %d.", n);
			Logger.warn(message);
			return String.format("Число %d не трехзначное.\n", n);
		}
		
		try {
			int number;
			for (int i = 1; i < 10; ++i) {
				for (int j = 0; j < 10; ++j) {
					for (int k = 0; k < 10; ++k) {
						number = i * 100 + j * 10 + k;
						if (n < number) throw new Exception();
						
						if ((i != j) && (i != k) && (j != k)) {
							res += String.format("%d ", number);	
						}
					}
				}
			}
			System.out.println();
		} catch (Exception e) {}
		
		return res;
	}
	
	/**
	 * Генерируте строку всех трехзначных чисул, у которых все цифры
	 * разные. Генерирует до n.
	 * 
	 * @param n последнее трехзначное число.
	 */
	public static String threeNumbersWhile(int n) {
		String res = "";
		if ((n < 100) || (n > 999)) {
			String message = String.format("Задание 3. Введено не трехзначное число: %d.", n);
			Logger.warn(message);
			return String.format("Число %d не трехзначное.\n", n);
		}
		
		try {
			int number, i = 1, j = 0, k = 0;
			while (i < 10) {
				while (j < 10) {
					while (k < 10) {
						number = i * 100 + j * 10 + k;
						if (n < number) throw new Exception();
						
						if ((i != j) && (i != k) && (j != k))
							res += String.format("%d ", number);
						++k;
					}
					k = 0;
					++j;
				}
				j = 0;
				++i;
			}
			System.out.println();
		} catch (Exception e) {}
		
		return res;
	}
	
	/**
	 * Выводит трехзначные числа с разными цифрами, которые меньше n.
	 * Вывод с помощью циклов for и while
	 */
	public static void numbersDisplay(int n) {
		if ((n < 100) || (n > 999)) {
			String message = String.format("Задание 3. Введено не трехзначное число: %d.", n);
			Logger.warn(message);
			System.out.printf("Число %d не трехзначное.\n", n);
		} else {
			System.out.println("Трехзначные числа через цикл for.");
			Numbers.threeNumbers(n);
			
			System.out.println("\nТрехзначные числа через цикл while.");
			Numbers.threeNumbersWhile(n);
		}
	}
}
