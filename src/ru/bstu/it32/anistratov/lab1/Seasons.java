package ru.bstu.it32.anistratov.lab1;

import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Seasons {
	static final Logger Logger = LogManager.getLogger();
	
	/** seasons массив времен года */
	TimeYear[] seasons;
	
	/** lipYear високосный год */
	private int lipYear = 2020;
	
	/** february кол-во дней в феврале */
	private int february = 28;
	
	public TimeYear[] getSeasons() {
		return seasons;
	}
	
	/**
	 * Инициализирует объект с массивом времен года. Определяет високосный ли
	 * текущий год, и записывает соответствующие кол-во дней в февраль.
	 */
	public Seasons() {
		// Получение текущего года.
		Date date = new Date();
		Calendar calendar = Calendar
				.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);

		// Определение кол-ва дней в феврале.
		if (year % lipYear == 0) february = 29;
		
		String name = "Зима";
		String[] months = {"Декабрь", "Январь", "Февраль"};
		int[] monthDays = {31, 31, february};
		TimeYear season = new TimeYear(name, months, monthDays);
		seasons = new TimeYear[4];
		seasons[0] = season;
		
		name = "Весна";
		months[0] = "Март"; months[1] = "Апрель"; months[2] = "Май";
		monthDays[0] = 31; monthDays[1] = 30; monthDays[2] = 31;
		season = new TimeYear(name, months, monthDays);
		seasons[1] = season;
		
		name = "Лето";
		months[0] = "Июнь"; months[1] = "Июль"; months[2] = "Август";
		monthDays[0] = 30; monthDays[1] = 31; monthDays[2] = 31;
		season = new TimeYear(name, months, monthDays);
		seasons[2] = season;
		
		name = "Осень";
		months[0] = "Сентябрь"; months[1] = "Октябрь"; months[2] = "Ноябрь";
		monthDays[0] = 30; monthDays[1] = 31; monthDays[2] = 30;
		season = new TimeYear(name, months, monthDays);
		seasons[3] = season;
		
		Logger.info("Создан объект класса Season.");
	}
	
	/**
	 * Выводит информацию о времени года по его номеру:
	 * 1-зима, 2-весна, 3-лето, 4-осень.
	 * 
	 * @param idSeason номер времени года
	 */
	public void displaySeason(int idSeason) {
		switch (idSeason) {
		case 1:
			seasons[0].displayInfo();
			break;

		case 2:
			seasons[1].displayInfo();
			break;
		
		case 3:
			seasons[2].displayInfo();
			break;
		
		case 4:
			seasons[3].displayInfo();
			break;
		
		default:
			String message = String.format("Задание 2. Введено неверное время "
					+ "года: %d.", idSeason);
			Logger.warn(message);
			System.out.println("Неверный номер времени года.");
		}
	}
}