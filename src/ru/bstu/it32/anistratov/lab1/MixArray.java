package ru.bstu.it32.anistratov.lab1;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MixArray {
	static final Logger Logger = LogManager.getLogger();
	
	/** array массив с четным количеством элементов */
	private int[] array;
	
	public int[] getArray() {
		return array;
	}
	
	@Override
	public String toString() {
		String res = "";
		for (int i : array) {
			res += String.format("%d ", i);
		}
		return res;
	}

	/** Инициализирует объект данными по умолчанию. */
	public MixArray() {
		array = new int[2];
	}
	
	/**
	 * Инициализирует данные объекта. Если кол-во элементов у переданного
	 * массива не четное, то поле array инициализируется по умолчанию. 
	 * */
	public MixArray(int[] array) {
		Logger.info("Создан объект класса MixArray");
		this.array = Arrays.copyOf(array, array.length);
	}
	
	/** Меняет местами рядом стоящие элементы массива. */
	public void mixing() {
		int count = (array.length % 2 == 0) ? array.length: array.length - 1; 
			
		for (int i = 1; i <= count; i += 2) {
			int buf = array[i];
			array[i] = array[i - 1];
			array[i - 1] = buf;
		}
	}
	
	/** Выводит массив в консоль. */
	public void displayInfo() {
		for (int i : array) {
			System.out.printf("%d, ", i);
		}
		
		System.out.println();
	}
	
	/**
	 * Считывает массив с консоли.
	 * 
	 * @param in объект класса Scanner
	 */
	public void getConsoleArray(Scanner in) {
		int length;
				
		for (;;) {
			System.out.print("Введите кол-во элементов массива: ");
			
			if (in.hasNextInt()) {
				length = in.nextInt();
				
				if (length > 0) {
					break;
				}
			}
			
			System.out.println("Введите целое число.");
			in.nextLine();
		}
		
		array = new int[length];
		for (int i = 0; i < length; ++i) {
			for (;;) {
				System.out.printf("Введите %d элемент массива: ", i);
				if (in.hasNextInt()) {
					array[i] = in.nextInt();
					break;
				}
				
				System.out.println("Введите натуральное число.");
				in.nextLine();
			}
		}
	}
	
	/**
	 * Считывает массив целых чисел с файла. Первый элемент файла -
	 * длина массива.
	 * 
	 * @param filePath путь к файлу
	 */
	public void getFileArray(String filePath) {
		try (FileReader reader = new FileReader(new File(filePath));
				Scanner scanner = new Scanner(reader)) {
			if (!scanner.hasNextInt()) {
				String buf, message;
				
				buf = scanner.nextLine();
				message = String.format("Задание 4. Файл %s содержит "
						+ "некорректные данные: %s", filePath, buf);
				throw new Exception(message);
			}
			
			int length = scanner.nextInt();
			array = new int[length];
			
			length = 0;
			while (scanner.hasNextInt()) {
				array[length++] = scanner.nextInt();
			}
		} catch (Exception ex) {
			Logger.error(ex.getMessage(), ex);
			System.out.println(ex.getMessage());
		}	
	}
	
	public void getData(String strArray) {
		Scanner scan = new Scanner(strArray);
		ArrayList<Integer> arrList = new ArrayList<Integer>();
		
		while(scan.hasNextInt()) {
			arrList.add(scan.nextInt());
		}
		
		array = new int[arrList.size()];
		for (int i = 0; i < arrList.size(); ++i)
			array[i] = arrList.get(i);
		
		scan.close();
	}
	
	/**
	 * Считывает массив целых чисел с файла. Первый элемент файла -
	 * длина массива.
	 * 
	 * @param filePath путь к файлу
	 */
	public void getFileArrayFX(String filePath) {
		try (FileReader reader = new FileReader(new File(filePath));
				Scanner scanner = new Scanner(reader)) {
			if (!scanner.hasNextInt()) {
				String buf, message;
				
				buf = scanner.nextLine();
				message = String.format("Задание 4. Файл %s содержит "
						+ "некорректные данные: %s", filePath, buf);
				throw new Exception(message);
			}
			
			int length = scanner.nextInt();
			array = new int[length];
			
			length = 0;
			while (scanner.hasNextInt()) {
				array[length++] = scanner.nextInt();
			}
		} catch (Exception ex) {
			Logger.error(ex.getMessage(), ex);
		}	
	}
}
