package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public class Camera extends ScreenDevice {
	protected String matrixType;
	protected int xMatrixSize;    // Ширина матрицы
	protected int yMatrixSize;    // Длина матрицы
	protected int bottomFocus; // Нижняя граница фокусного расстояния
	protected int topFocus;    // Верхняя граница фокусного расстояния
	
	public String getMatrixType() {
		return matrixType;
	}

	public void setMatrixType(String matrixType) {
		this.matrixType = matrixType;
	}

	public int getxMatrixSize() {
		return xMatrixSize;
	}

	/*
	 * Устанавливает значение поля xMatrixSize. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param xMatrixSize ширина матрицы
	 */
	public boolean setxMatrixSize(int xMatrixSize) {
		if (xMatrixSize >= 0) {
			this.xMatrixSize = xMatrixSize;
			return true;
		}
		return false;
	}

	public int getyMatrixSize() {
		return yMatrixSize;
	}

	/*
	 * Устанавливает значение поля yMatrixSize. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param yMatrixSize длина матрицы
	 */
	public boolean setyMatrixSize(int yMatrixSize) {
		if (yMatrixSize >= 0) {
			this.yMatrixSize = yMatrixSize;
			return true;
		}
		return false;
	}

	public double getBottomFocus() {
		return bottomFocus;
	}

	/*
	 * Устанавливает значение поля bottomFocus. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param bottomFocus нижняя граница фокуса
	 */
	public boolean setBottomFocus(int bottomFocus) {
		if (bottomFocus >= 0) {
			this.bottomFocus = bottomFocus;
			return true;
		}
		return false;
	}

	public double getTopFocus() {
		return topFocus;
	}

	/*
	 * Устанавливает значение поля setTopFocus. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param setTopFocus верхняя граница фокуса
	 */
	public boolean setTopFocus(int topFocus) {
		if (topFocus >= 0) {
			this.topFocus = topFocus;
			return true;
		}
		return false;
	}

	@Override
	public int getCost() {
		return cost;
	}

	@Override
	public boolean canBuy(int cost) {
		if (cost >= this.cost)
			return true;
		return false;
	}
	
	@Override
	public void init(Scanner scanner) {
		System.out.println("Тип товара: фотоаппарат.");
		super.init(scanner);
		
		System.out.print("Введите тип матрицы фотоаппарата: ");
		matrixType = scanner.nextLine();
		
		System.out.print("Введите ширину матрицы (в мм.): ");
		xMatrixSize = readNumber(scanner);
		
		System.out.print("Введите длину матрицы (в мм.): ");
		yMatrixSize = readNumber(scanner);
		
		System.out.print("Введите нижнюю границу фокусного расстояния (в мм.): ");
		bottomFocus = readNumber(scanner);
		
		System.out.print("Введите верхнюю границу фокусного расстояния (в мм.): ");
		topFocus = readNumber(scanner);
	}

	@Override
	public String toString() {
		return String.format("Вид товара: камера.\n%s\nТип матрицы - %s,"
				+ " размер матрицы - %dх%d мм., фокусное расстояние - %d - %d"
				+ " мм.\n", super.toString(), matrixType, xMatrixSize,
				yMatrixSize, bottomFocus, topFocus);
	}
}
