package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Введите кол-во товаров.");
		int count = readCount(scanner);
		Product[] products = new Product[count];
		
		// считывание данных о товарах
		for (int i = 0; i < count; ++i) {
			System.out.println("\nТипы товаров: 1-Игрушка, 2-Сыр, 3-Сметана,"
					+ " 4-Камера, 5-Телевизор.");
			System.out.println("Введите тип товара.");
			int number = readCount(scanner);
			
			switch (number) {
			case 1:
				products[i] = new Toy();
				products[i].init(scanner);
				break;
			case 2:
				products[i] = new Cheese();
				products[i].init(scanner);
				break;
			case 3:
				products[i] = new SourCream();
				products[i].init(scanner);
				break;
			case 4:
				products[i] = new Camera();
				products[i].init(scanner);
				break;
			case 5:
				products[i] = new TV();
				products[i].init(scanner);
				break;
			default:
				System.out.println("Нет такого типа товара!");	
			}
		}

		// поиск товара с минимальной стоимостью
		Product minProduct = products[0];
		if (count > 1) {
			for (int i = 0; i < count; i++) {
				if (minProduct.getCost() > products[i].getCost())
					minProduct = products[i];
			}			
		}
		
		System.out.println("\nТовар с наименьшей стоимостью.");
		System.out.print(minProduct);
		scanner.close();
	}

	/*
	 * Считывает кол-во товаров. Может принимать строку, из которой
	 * будет получено число.
	 */
	private static int readCount(Scanner scanner) {
		int res = 0;
		boolean find = true;
		
		while (find) {
			System.out.print("Введите целое число: ");
			String line = scanner.nextLine();
			Pattern pattern = Pattern.compile("\\d+");
			Matcher matcher = pattern.matcher(line);
			
			if (matcher.find()) {
				res = Integer.parseInt(line.substring(matcher.start(), matcher.end()));
				if (res > 0) {
					find = false;
					break;
				}
			}
		}
		
		return res;
	}
}
