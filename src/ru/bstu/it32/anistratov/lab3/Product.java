package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

/**
 * Абстрактный класс товара. Содержит поля, необходимые
 * для представления товара. 
 */
public abstract class Product {
	protected String name;
	protected String producer;
	protected int cost;
	protected double weight;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getProducer() {
		return producer;
	}
	
	public void setProducer(String producer) {
		this.producer = producer;
	}
	
	
	/**
	 * Устанавливает стоимость товара. При успешной работе возвращает true,
	 * в противном случае - false.
	 * 
	 * @param cost стоимость товара
	 */
	public boolean setCost(int cost) {
		if (cost > 0) {
			this.cost = cost;
			return true;
		}
		return false;
	}
	
	public double getWeight() {
		return weight;
	}
	
	/**
	 * Устанавливает массу товара. При успешной работе возвращает true,
	 * в противном случает - false.
	 * 
	 * @param weight масса товара
	 */
	public boolean setWeight(double weight) {
		if (weight > 0) {
			this.weight = weight;
			return true;
		}
		return false;
	}
	
	/**
	 * Инициализирует объект. Производит считывание данных о товаре
	 */
	public void init(Scanner scanner) {
		System.out.print("Введите название: ");
		name = scanner.nextLine();
		
		System.out.print("Введите название производителя: ");
		producer = scanner.nextLine();
		
		System.out.print("Введите стоимость (в рублях): ");
		cost = readNumber(scanner);
		
		System.out.print("Введите массу (в кг.): ");
		weight = readDNumber(scanner);
	}
	
	/**
	 * Возвращает стоимость товара.
	 */
	public abstract int getCost();
	
	/**
	 * Определяет, можно ли купить товар за указанную стоимость.
	 * 
	 * @param cost желаемая стоимость покупки
	 */
	public abstract boolean canBuy(int cost);
	
	/**
	 * Считывает целое число с консоли.
	 */
	public static int readNumber(Scanner scanner) {
		int number = 0;
		String buf = "";
		
		while (true) {
			buf = scanner.nextLine();
			
			try {
				number = Integer.parseInt(buf);
				break;
			}
			catch (Exception e) {
				System.out.print("Ввели некорректное число. Попробуйте еще раз: ");
			}
		}
		return number;
	}
	
	/**
	 * Считывает вещественное число с консоли.
	 */
	public static double readDNumber(Scanner scanner) {
		double number = 0;
		String buf = "";
		
		while (true) {
			buf = scanner.nextLine();
			
			try {
				number = Double.parseDouble(buf);
				break;
			}
			catch (Exception e) {
				System.out.print("Ввели некорректное число. Попробуйте еще раз: ");				
			}
		}
		return number;
	}
}
