package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public class Toy extends Product{
	protected int ageLimit;
	
	public int getAgeLimit() {
		return ageLimit;
	}

	/*
	 * Устанавливает значение поля ageLimit. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param ageLimit возрастное ограничение
	 */
	public boolean setAgeLimit(int ageLimit) {
		if (ageLimit >= 0) {
			this.ageLimit = ageLimit;
			return true;
		}
		return false;
	}

	@Override
	public void init(Scanner scanner) {
		System.out.println("Тип товара: игрушка.");
		super.init(scanner);
		
		System.out.print("Введите возрастное ограничение: ");
		ageLimit = readNumber(scanner);
	}

	@Override
	public int getCost() {
		return cost;
	}

	@Override
	public boolean canBuy(int cost) {
		if (cost < this.cost)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String str = String.format("Вид товара: игрушка\nНазвание - %s, "
				+ "стоимость - %d р., возрастное ограничение - %d+\n"
				+ "Производитель - %s, масса - %.3f кг.\n", name, cost, ageLimit,
				producer, weight);
		return str;
	}
}
