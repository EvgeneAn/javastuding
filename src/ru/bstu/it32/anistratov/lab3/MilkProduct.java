package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public abstract class MilkProduct extends Product {
	protected double proteins;       // Белки
	protected double fats;           // Жиры
	protected double  carbohydrates; // Углеводы
	protected int calorie;           // Калорийность
	protected int maxStorTmp;        // Максимальная температура хранения
	protected int minStorTmp;        // Минимальная температура хранения
	protected int shelfLife;         // Срок годности
	
	public double getProteins() {
		return proteins;
	}

	/*
	 * Устанавливает значение поля proteins. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param proteins белки
	 */
	public boolean setProteins(double proteins) {
		if (proteins >= 0) {
			this.proteins = proteins;
			return true;
		}
		return false;
	}

	public double getFats() {
		return fats;
	}

	/*
	 * Устанавливает значение поля fats. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param fats жиры
	 */
	public boolean setFats(double fats) {
		if (fats >= 0) {
			this.fats = fats;
			return true;
		}
		return false;
	}

	public double getCarbohydrates() {
		return carbohydrates;
	}

	/*
	 * Устанавливает значение поля carbohydrates. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param carbohydrates углеводы
	 */
	public boolean setCarbohydrates(double carbohydrates) {
		if (carbohydrates >= 0) {
			this.carbohydrates = carbohydrates;
			return true;
		}
		return false;
	}

	public int getCalorie() {
		return calorie;
	}

	/*
	 * Устанавливает значение поля calorie. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param calorie калории
	 */
	public boolean setCalorie(int calorie) {
		if (calorie >= 0) {
			this.calorie = calorie;
			return true;
		}
		return false;
	}

	public int getMaxStorTmp() {
		return maxStorTmp;
	}

	public void setMaxStorTmp(int maxStorTmp) {
		this.maxStorTmp = maxStorTmp;
	}

	public int getMinStorTmp() {
		return minStorTmp;
	}

	public void setMinStorTmp(int minStorTmp) {
		this.minStorTmp = minStorTmp;
	}

	public int getShelfLife() {
		return shelfLife;
	}

	/*
	 * Устанавливает значение поля shelfLife. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param shelfLife срок годности
	 */
	public boolean setShelfLife(int shelfLife) {
		if (shelfLife >= 0) {
			this.shelfLife = shelfLife;
			return true;
		}
		return false;
	}

	public void init(Scanner scanner) {
		super.init(scanner);
		
		System.out.print("Введите кол-во белков (в г. на 100 г.): ");
		proteins = readNumber(scanner);
		
		System.out.print("Введите кол-во жиров (в г. на 100 г.): ");
		fats = readNumber(scanner);
		
		System.out.print("Введите кол-во углеводов (в г. на 100 г.): ");
		carbohydrates = readNumber(scanner);
		
		System.out.print("Введите калорийность (в Ккал): ");
		calorie = readNumber(scanner);
		
		System.out.print("Введите минимальную температуру хранения"
				+ " (в Цельсиях): ");
		minStorTmp = readNumber(scanner);
		
		System.out.print("Введите максимальную температуру хранения"
				+ " (в Цельсиях): ");
		maxStorTmp = readNumber(scanner);
		
		System.out.print("Введите срок годности (в сутках): ");
		shelfLife = readNumber(scanner);
	}
	
	@Override
	public String toString() {
		String str = String.format("Название - %s, производитель - %s, "
				+ "стоимость - %d р.\nСрок годности: %d, хранить при %d, %d.",
				name, producer, cost, shelfLife, minStorTmp, maxStorTmp);
		return str;
	}
}
