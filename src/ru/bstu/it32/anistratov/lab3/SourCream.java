package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public class SourCream extends MilkProduct {
	protected double fatContent; // Жирность (%)
	
	public double getFatContent() {
		return fatContent;
	}

	/*
	 * Устанавливает значение поля fatContent. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param fatContent жирность сметаны
	 */
	public boolean setFatContent(double fatContent) {
		if (fatContent >= 0) {
			this.fatContent = fatContent;
			return true;
		}
		return false;
	}

	@Override
	public void init(Scanner scanner) {
		System.out.println("Вид товара: сметана.");
		super.init(scanner);
		
		System.out.print("Введите жирность сметаны (в %): ");
		fatContent = readDNumber(scanner);
	}

	@Override
	public int getCost() {
		return cost;
	}

	@Override
	public boolean canBuy(int cost) {
		if (cost >= this.cost)
			return true;
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("Вид товара: сметана.\n%s Жирность - %.2f\n",
				super.toString(), fatContent);
	}
}
