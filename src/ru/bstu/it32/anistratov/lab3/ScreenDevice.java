package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public abstract class ScreenDevice extends Product {
	protected String screenType;  // Тип матрицы экрана (IPS, TN и т.д.)
	protected int xResolution;    // Разрешение экрана по горизонтали
	protected int yResolution;    // Разрешение экрана по вертикали
	
	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
			this.screenType = screenType;
	}

	public int getxResolution() {
		return xResolution;
	}

	/*
	 * Устанавливает значение поля xResolution. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param xResolution горизонтальное разрешение
	 */
	public boolean setxResolution(int xResolution) {
		if (xResolution >= 0) {
			this.xResolution = xResolution;
			return true;
		}
		return false;
	}

	public int getyResolution() {
		return yResolution;
	}

	/*
	 * Устанавливает значение поля yResolution. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param yResolution вертикальное разрешение
	 */
	public boolean setyResolution(int yResolution) {
		if (yResolution >= 0) {
			this.yResolution = yResolution;
			return true;
		}
		return false;
	}

	@Override
	public void init(Scanner scanner) {
		super.init(scanner);
		
		System.out.print("Введите разрешение экрана по горизонтали: ");
		xResolution = readNumber(scanner);
		
		System.out.print("Введите разрешение экрана по вертикали: ");
		yResolution = readNumber(scanner);
	}
	
	@Override
	public String toString() {
		String str = String.format("Название - %s, производитель - %s, "
				+ "стоимость - %d р.\nТип матрицы экрана: %s, разрешение -  %dх%d.",
				name, producer, cost, screenType, xResolution, yResolution);
		return str;
	}
}
