package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public class TV extends ScreenDevice {
	protected String OS;
	protected int frameRate;
	protected int diagonal;
	
	public String getOS() {
		return OS;
	}

	public void setOS(String oS) {
		OS = oS;
	}

	public int getFrameRate() {
		return frameRate;
	}

	/*
	 * Устанавливает значение поля frameRate. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param frameRate частота обновления кадров
	 */
	public boolean setFrameRate(int frameRate) {
		if (frameRate >= 0) {
			this.frameRate = frameRate;
			return true;
		}
		return false;
	}

	public int getDiagonal() {
		return diagonal;
	}

	/*
	 * Устанавливает значение поля diagonal. При успешной работе
	 * возвращает - true, иначе - false.
	 * 
	 * @param diagonal диагональ экрана
	 */
	public boolean setDiagonal(int diagonal) {
		if (diagonal >= 0) {
			this.diagonal = diagonal;
			return true;
		}
		return false;
	}

	@Override
	public void init(Scanner scanner) {
		System.out.println("Тип товара: телевизор.");		
		super.init(scanner);
		
		System.out.print("Введите название ОС: ");
		OS = scanner.nextLine();
		
		System.out.print("Введите частоту обновления кадров (в Гц): ");
		frameRate = readNumber(scanner);
		
		System.out.print("Введите диагональ (в дюймах): ");
		diagonal = readNumber(scanner);
	}

	@Override
	public int getCost() {
		return cost;
	}
	
	@Override
	public boolean canBuy(int cost) {
		if (cost >= this.cost)
			return true;
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("Вид товара: телевизор.\n%s\nОС - %s, частота "
				+ "обновления кадров - %d(Гц), диагональ - %d\n",
				super.toString(), OS, frameRate, diagonal);
	}
}
