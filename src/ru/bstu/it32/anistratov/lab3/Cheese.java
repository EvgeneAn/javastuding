package ru.bstu.it32.anistratov.lab3;

import java.util.Scanner;

public class Cheese extends MilkProduct {
	protected String hardness; // Твердость сыра
	
	public String getHardness() {
		return hardness;
	}

	public void setHardness(String hardness) {
		this.hardness = hardness;
	}

	@Override
	public void init(Scanner scanner) {
		System.out.println("Вид товара: сыр.");		
		super.init(scanner);
		
		System.out.print("Введите твердость сыра: ");
		hardness = scanner.nextLine();
	}

	@Override
	public int getCost() {
		return cost;
	}

	@Override
	public boolean canBuy(int cost) {
		if (cost >= this.cost)
			return true;
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("Вид товара: сыр.\n%s Твердость - %s\n",
				super.toString(), hardness);
	}
}
