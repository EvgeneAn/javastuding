package ru.bstu.it32.anistratov.lab4;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		Scanner io = new Scanner(System.in);
		
		String filePath = "";
		File sourceFile;
		File fileOut;
		
		while (true) {
			System.out.print("Введите путь к файлу: ");
			filePath = io.nextLine();
			sourceFile = new File(filePath);
			
			if (sourceFile.exists()) {
				if (sourceFile.isFile()) {
					if (sourceFile.canRead()) break;
					else System.out.println("Файл " + filePath + "не может быть прочитан");
				}
				else System.out.println(filePath + " является каталогом");
			}
			else System.out.println("Файл или каталог не существует");
		}
		
		while (true) {
			System.out.print("Введите имя выходного файла: ");
			filePath = io.nextLine();
			fileOut = new File(filePath);
			
			if (!fileOut.exists()) {
				try {
					if (fileOut.createNewFile()) break;
					else System.out.println("Не удалось создать файл");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else System.out.println("Файл или каталог уже существует");
		}
		
		if (ParserUserData.readFile(sourceFile)) {
			try (FileWriter writer = new FileWriter(fileOut, false))
			{
				writer.write(ParserUserData.parse());
				System.out.println(ParserUserData.getParseUserData());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else System.out.println("Не удалось прочитать файл");
		
		io.close();
	}
}
