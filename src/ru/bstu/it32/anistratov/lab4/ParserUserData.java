package ru.bstu.it32.anistratov.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserUserData {
	private static String sourceFile;
	private static String parseUserData;
	
	public static String getFile() {
		return sourceFile;
	}

	public static void setFile(String file) {
		ParserUserData.sourceFile = file;
	}
	
	public static String getParseUserData() {
		return parseUserData;
	}
	
	/***
	 * Читает файл и сохраняет данные в строку
	 * @param fileIn входной файл
	 * @return возвращает true, если удалось прочитать файл, иначе false
	 */
	public static boolean readFile(File fileIn) {
		boolean result = false;
		try (Scanner file = new Scanner(fileIn)) {
			sourceFile = file.useDelimiter("\\A").next();
			result = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String parse() {
		String userData = "";
		String regexp = "([а-яa-z]+) ([а-яa-z]+) ([а-яa-z]+ )"
				+ "([7-8]\\d{10}) ([a-zа-я_\\.]+) ((\\d{2}\\.){2}\\d{4})";
		Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE
										  | Pattern.MULTILINE | Pattern.UNICODE_CASE);
		Matcher matcher = pattern.matcher(sourceFile);
		
		while (matcher.find()) {
			String data = String.format(
					"Фамилия - %s\nИмя - %s\nОтчество - %s\nНомер телефона - %s"
					+ "\nНик - %s\nДата рождения - %s\n", matcher.group(1),
					matcher.group(2), matcher.group(3), matcher.group(4),
					matcher.group(5), matcher.group(6));
			userData += data + "--------------\n";
		}

		parseUserData = userData;
		return userData;
	}

}
